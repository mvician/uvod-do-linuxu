#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
control="$base_dir/.control.sh $base_name"
result="$base_dir/.result.sh $base_name"

STEPS=("runabs" "runhome")

$control runcur
if [ $? -ne 0 ]; then
	if [ "$(pwd)" != "$base_dir" ]; then
		echo "Prosím, přepněte se pomocí příkazu cd do adresáře s tímto skriptem: ~/uvod-do-linuxu"
		echo "A zadejte znovu příkaz: ./05-run.sh"
		exit 1
	fi
	if [ "$0" == "./$base_name" ]; then
		echo "Výtečně!"
		$control runcur TRUE
	else
		echo "Prosím, spusťte tento program z adresáře skriptu pomocí relativní cesty"
		echo "tedy: ./$base_name"
		exit 1
	fi
fi

$control runabs
if [ $? -ne 0 ]; then
	if [ "$0" == "$base_dir/$base_name" ]; then
		echo "Výtečně!"
		$control runabs TRUE
	else
		echo "Prosím, spusťte tento program pomocí absolutní cesty"
		echo "tedy: $base_dir/$base_name"
		echo "TIP: nezpomeňte na doplnovaní TABem"
		exit 1
	fi
fi

$control runhome
if [ $? -ne 0 ]; then
	if [ "$(pwd)" != "$HOME" ]; then
		echo "Prosím, přepněte se pomocí příkazu cd do tvého domovského adresáře"
		echo "A zadejte znovu příkaz: uvod-do-linuxu/05-run.sh"
		exit 1
	fi
	expected=$(echo $base_dir | sed "s|$HOME/||g")
	if [ "$0" == "$expected/$base_name" ] || [ "$0" == "./$expected/$base_name" ]; then
		echo "Výtečně!"
		$control runhome TRUE
	else
		echo "Prosím, spusťte tento program z tvého domovského adresáře pomocí relativní cesty"
		echo "tedy: $expected/$base_name"
		exit 1
	fi
fi

$result
