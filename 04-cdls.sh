#!/bin/bash

base_dir=$(realpath $(dirname $0))
base_name=$(basename $0)
control="$base_dir/.control.sh $base_name"
result="$base_dir/.result.sh $base_name"

STEPS=("cdbase" "cdgjj")

$control cdbase
if [ $? -ne 0 ]; then
	if [ "$(pwd)" != "$base_dir" ]; then
		echo "Prosím, přepněte se pomocí příkazu cd do adresáře ~/uvod-do-linuxu"
		echo "A zadejte znovu příkaz: ctvrty"
		echo "TIP: nezpomeňte na doplnovaní TABem"
		exit 1
	fi
	echo "Výborně, jste v adresáři uvod-do-linuxu"
	$control cdbase TRUE
fi

$control cdgjj
if [ $? -ne 0 ]; then
	expected="$base_dir/GjjgtyTX3T8mlawz30acPnoShU"
	if [ "$(pwd)" != "$expected" ]; then
		where=""
		if [ "$(pwd)" != "$base_dir" ]; then
			where="~/uvod-do-linuxu"
		fi
		echo "Prosím, přepněte se pomocí příkazu cd do adresáře Gjj<TAB>"
		echo "(Adresář začínající na Gjjg...)"
		echo "A zadejte znovu příkaz: ctvrty"
		exit 1
	fi
	echo "Výborně, jste v adresáři $base_dir/GjjgtyTX3T8mlawz30acPnoShU"
	$control cdgjj TRUE
fi

$control cdhome
if [ $? -ne 0 ]; then
	expected="$HOME"
	if [ "$(pwd)" != "$expected" ]; then
		echo "Prosím, přepněte se pomocí příkazu cd do vašho domovského adresáře"
		echo "A zadejte znovu příkaz: ctvrty"
		exit 1
	fi
	echo "Výborně, jste ve svém domovském adresáři"
	$control cdhome TRUE
fi

$result
