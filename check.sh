#!/bin/bash

install=0
if [ $# -ge 1 ]; then
	if [ "$1" == "install" ]; then
		echo "(Install mode)"
		install=1
	fi
fi


programs=("git" "figlet" "openssl" "wget" "sudo" "realpath" "gnome-terminal" )
missing=""

expected_lsb_id="Ubuntu"
expected_lsb_release="16.04"

programs_ok=0
aliases_ok=0

for program in ${programs[@]}; do 
	which $program 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
		missing="$missing $program"
	fi
done
if [ "$missing" ]; then
	echo "Chybí programy$missing"
	if [ $install -eq 1 ]; then
		echo "Instaluji ..."
		sudo apt -y install$missing
		if [ $? -eq 0 ]; then
			echo "OK, všechny progrmay správně nainstalovány."
		else
			echo "FAILED!"
		fi
	else
		echo "Nainstalujte je, například pomocí:"
		echo "sudo apt install$missing"
	fi
else
	echo "Všecny programy nutné pro správný běh cvičení jsou nainstalovány."
	programs_ok=1
fi

if [ "$(which bash)" != "$SHELL" ]; then
	echo ""
	echo "VAROVÁNÍ: Nepoužíváte $(which bash) jako svůj defaultní shell - některé cvičení nemusí fungovat správně, nebo nebudou fungovat jejich nápovědy".
fi

base_dir="$(realpath $(dirname $0))"

if [ "$base_dir" != "$(realpath ~/uvod-do-linuxu)" ]; then
	echo ""
	echo "VAROVÁNÍ: adresář se cvičeními nemá stejné umístění jako na kurzu (~/uvod-do-linuxu) - zadání cvičení nebude stoprocentně odpovídat."
	#if [ $install -eq 1 ]; then
	#	mv $base_dir $HOME/uvod-do-linuxu
	#fi
fi

which lsb_release 1>/dev/null 2>/dev/null
if [ $? -eq 0 ]; then
	lsb_id=$(lsb_release -s -i)
	lsb_release=$(lsb_release -s -r)
else
	lsb_id="unknown"
	lsb_release="unknown"
fi

if [ "$expected_lsb_id" != "$lsb_id" ]; then
	echo ""
	echo "VAROVÁNÍ: Nepoužíváte Ubuntu - některé cvičení nemusí fungovat správně."
else
	if [ "$expected_lsb_release" != "$lsb_release" ]; then
		echo ""
		echo "VAROVÁNÍ: Nepoužíváte verzi Ubuntu 16.04 pro kterou byly cvičení optimalizovány - některé cvičení nemusí fungovat správně."
	fi
fi

aliases_source="$base_dir/.bash_aliases"
aliases_comment="CZNICUDL"
aliases_source_ok=1
bashrc="$HOME/.bashrc"
#for alias in $(grep alias $aliases_source | awk -F'[ =]' '{print $2}'); do
#	#echo "- $alias"
#	type $alias 1>/dev/null 2>/dev/null
#	if [ $? -ne 0 ]; then
#		aliases_source_ok=0
#	fi
#done
grep $aliases_comment $bashrc 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
	aliases_source_ok=0
fi
if [ $aliases_source_ok -ne 1 ]; then
	echo ""
	echo "VAROVÁNÍ: Chybí aliasy! První čtyři cvičení nebudou fungovat!"
	if [ $install -eq 1 ]; then
		echo -n "Konfiguruji... "
		echo "source $aliases_source #$aliases_comment" >> $bashrc
		if [ $? -eq 0 ]; then
			echo "OK"
		else
			echo "FAILED!!!"
		fi
	else
		echo "Opravíte vložením řádku 'source $aliases_source #$aliases_comment' nakonec souboru '~/.bashrc'"
	fi
fi

desktop_ok=0
desktop_dir="$HOME/Plocha"
if [ -d "$HOME/Desktop" ]; then
	desktop_dir="$HOME/Desktop"
fi
desktop_name="uvod-do-linuxu.desktop"
desktop_cv="$desktop_dir/$desktop_name"
if [ ! -f $desktop_cv ]; then
	echo "Chybí vám odkaz na ploše!"
	if [ $install -eq 1 ]; then
		echo -n "Vytvářím..."
		cp $base_dir/$desktop_name $desktop_cv
		if [ $? -eq 0 ]; then
			echo "OK"
		else
			echo "FAILED!"
		fi
	fi
fi

if [ -f $desktop_cv ]; then
	grep USERNAME $desktop_cv 1>/dev/null 2>/dev/null
	if [ $? -eq 0 ]; then
		echo "Odkaz na ploše pro první cvičení nemá správnou cestu."
		if [ $install -eq 1 ]; then
			echo -n "Opravuji..."
			sed -i "s/USERNAME/$USER/g" $desktop_cv
			if [ $? -eq 0 ]; then
				echo "OK"
			else
				echo "FAILED!"
			fi
		fi
	else
		echo "Odkaz na ploše existuje a je správný."
	fi
fi

if [ $programs_ok -eq 1 ] && [ $aliases_ok -eq 1 ] && [ $desktop_ok -eq 1 ]; then
	figlet -f big OK
fi
