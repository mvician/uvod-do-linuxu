#!/bin/bash

which git 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
	echo "Installing git"
	which apt 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then
		echo "Používáte nepodporovaný systém! Nainstalujte si git sami."
		exit 1
	fi
	sudo apt install git
fi

dir="$HOME/uvod-do-linuxu"
if [ ! -d "$dir" ]; then
	mkdir $dir
	if [ $? -ne 0 ]; then
		echo "Nedaří se vytvořit složku $dir"
		exit 1
	fi
	echo "Stahuji cvičení"
	git clone https://gitlab.labs.nic.cz/mvician/uvod-do-linuxu $dir
else
	echo "Cvičení již stažena, aktualizuji..."
	cd $dir
	git pull
fi

cd $dir
bash $dir/check.sh install
