#!/bin/bash

base_dir="$(realpath $(dirname $0))"

exercises="$(ls $base_dir/*-*.sh)"

exercise_done=0
exercise_count=0

ignored="06b-proxy-ipv6.sh 11-background.sh"

for exercise in ${exercises[@]}; do
	name=$(basename $exercise)
	echo $ignored | grep $name 1>/dev/null 2>/dev/null
	if [ $? -eq 0 ]; then
		continue
	fi
	echo -ne "$name\t"
	$base_dir/.result.sh $name 1>/dev/null 2>/dev/null
	if [ $? -eq 0 ]; then
		echo " - OK"
		let exercise_done++
	else
		echo " - zatím nesplněno"
	fi
	let exercise_count++
done

echo ""
echo "Splněno: $exercise_done/$exercise_count = $(expr $exercise_done \* 100 / $exercise_count)%"
