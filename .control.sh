#!/bin/bash

base_dir="$(realpath $(dirname $0))"

if [ $# -ne 2 ] && [ $# -ne 3 ]; then
	echo "ERROR! Wrong parameters!"
	echo "usage: $0 namespace variable [TRUE]"
	exit 1
fi

namespace=$1
variable=$2

file="$base_dir/.$namespace.control"

if [ ! -f "$file" ]; then
	touch $file
	if	[ $? -ne 0 ]; then
		echo "ERROR: Cannot create file $file!"
		exit 1
	fi
fi

if [ $# -eq 3 ]; then # Set variable
	value=$3
	if [ "$value" != "TRUE" ]; then
		echo "Error! Third parameter isn't TRUE!"
		exit 1
	fi
	grep $variable $file 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then # Not found
		echo $variable >> $file
	fi
else # Get variable
	grep $variable $file 1>/dev/null 2>/dev/null
	if [ $? -ne 0 ]; then # Not found
		exit 1
	fi
fi
